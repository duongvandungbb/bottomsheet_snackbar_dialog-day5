import 'package:dialog_bottomsheet/screens/fourth.dart';
import 'package:dialog_bottomsheet/screens/third.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:dialog_bottomsheet/screens/second.dart';

class Home extends StatelessWidget {
  goToNext() {
    navigator.push(MaterialPageRoute(builder: (context) => Second()));
    //Get.to(Second());
  }

  _showSnackBar() {
    Get.snackbar(
      "Hey Dung",
      "This is Snackbar",
      snackPosition: SnackPosition.BOTTOM,
    );
  }

  _showDialog() {
    Get.defaultDialog(
      title: "Dialog",
      content: Text("Welcome Dung !"),
    );
  }

  _showBottomSheet() {
    Get.bottomSheet(
      Container(
        child: Wrap(
          children: <Widget>[
            ListTile(
              leading: Icon(Icons.music_note),
              title: Text('Second Screen'),
              onTap: () => Get.offAll(Second()),
            ),
            ListTile(
              leading: Icon(Icons.videocam),
              title: Text('Third Screen'),
              onTap: () => Get.offAll(Third()),
            ),
            ListTile(
              leading: Icon(Icons.videocam),
              title: Text('Fourth Screen'),
              onTap: () => Get.offAll(Fourth()),
            ),
            SizedBox(
              height: 100,
            ),
          ],
        ),
      ),
      backgroundColor: Colors.white,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Home"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              child: Text("Screen Second"),
              onPressed: () => goToNext(),
            ),
            RaisedButton(
              child: Text("Snackbar"),
              onPressed: _showSnackBar,
            ),
            RaisedButton(
              child: Text("Dialog"),
              onPressed: _showDialog,
            ),
            RaisedButton(
              child: Text("Bottom Sheet"),
              onPressed: _showBottomSheet,
            ),
            SizedBox(
              height: 40,
            ),
          ],
        ),
      ),
    );
  }
}
